#!/bin/bash
# See http://www.iso.org/iso/country_codes/iso_3166_code_lists/country_names_and_code_elements.htm
# for ISO codes to use below
# -------------------------------------------------------------------------------
ISO="cn ru fr ng pk ph th tr kp kr"
 
### Set PATH ###
IPT=/sbin/iptables
WGET=/usr/bin/wget
 
### No editing below ###
SPAMLIST="countrydrop"
ZONEROOT="/root/iptables/lists"
DLROOT="http://www.nirsoft.net/countryip"
 
cleanOldRules(){
$IPT -F
$IPT -X
$IPT -t nat -F
$IPT -t nat -X
$IPT -t mangle -F
$IPT -t mangle -X
$IPT -P INPUT ACCEPT
$IPT -P OUTPUT ACCEPT
$IPT -P FORWARD ACCEPT
}
 
# create a dir
[ ! -d $ZONEROOT ] && /bin/mkdir -p $ZONEROOT
 
# clean old rules
cleanOldRules
 
# create a new iptables list
$IPT -N $SPAMLIST
 
for c  in $ISO
do
	# local zone file
	tDB=$ZONEROOT/$c.csv
 
	# get fresh zone file
	$WGET -O $tDB $DLROOT/$c.csv
 
	# country specific log message
	SPAMDROPMSG="$c Country Drop"
 
	# get 
	# input list is CSV formatted, using awk to split col of ranges and then reformat for IPTABLES range input
	BADIPS=$(awk -F "," -v s=1 -v e=2 '{print $s"-"$e}' $tDB)
	for ipblock  in $BADIPS
	do
	   $IPT -A $SPAMLIST -m iprange --src-range $ipblock -j LOG --log-prefix "$SPAMDROPMSG"
	   $IPT -A $SPAMLIST -m iprange --src-range $ipblock -j DROP
	done
done
 
# Drop everything 
$IPT -I INPUT -j $SPAMLIST
$IPT -I OUTPUT -j $SPAMLIST
$IPT -I FORWARD -j $SPAMLIST
 
exit 0
